package com.example.bookmarked;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.io.File;



public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    private ImageView ivCover;
    private TextView tvTitle;
    private TextView tvAuthor;
    private Button btnRead;
    //private Button btnDNF;
    Context context;

    ButtonClickListener btnC;

    public ItemViewHolder(@NonNull View itemView, ButtonClickListener listener) {
        super(itemView);
        ivCover=itemView.findViewById(R.id.ivCover);
        tvTitle=itemView.findViewById(R.id.tvTitle);
        tvAuthor=itemView.findViewById(R.id.tvAuthor);
        btnRead=(Button) itemView.findViewById(R.id.btnRead);
        btnC=listener;
        //btnDNF=itemView.findViewById(R.id.btnDNF);
        context=itemView.getContext();
        btnRead.setOnClickListener(this::onClick);
    }

    public void setItems(Book book){
        tvTitle.setText(book.getTitle());
        tvAuthor.setText(book.getAuthor());
        String temp=book.getImageLink();
        int id=context.getResources().getIdentifier(temp,"drawable", context.getPackageName());
        ivCover.setImageResource(id);

    }

    @Override
    public void onClick(View v) {
        btnC.onClick(getAdapterPosition());
    }
}
