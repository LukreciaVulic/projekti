package com.example.bookmarked;

public class Book {
    private String author;

    private String imageLink;

    private String title;

    public Book(String author,String image, String title){
        this.author=author;
        this.imageLink=image;
        this.title=title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }



}

