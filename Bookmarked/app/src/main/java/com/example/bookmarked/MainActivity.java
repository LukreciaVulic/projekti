package com.example.bookmarked;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity implements ButtonClickListener{
    private FragmentAdapter  mAdapter;
    private ViewPager mViewPager;
    private RandomFragment mRandomFragment;
    private BooksFragment mBookFragment;
    private ListFragment mReadFragment;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initialize();
    }

    private void initialize() {
        mAdapter=new FragmentAdapter(getSupportFragmentManager());
        mViewPager=findViewById(R.id.viewPager);
        setUpPager(mViewPager);
        TabLayout tabLayout=findViewById(R.id.tab);
        tabLayout.setupWithViewPager(mViewPager);
    }

    private void setUpPager(ViewPager mViewPager) {
        mRandomFragment=new RandomFragment();
        mReadFragment=new ListFragment();
        mBookFragment= new BooksFragment(mReadFragment);
        mAdapter.addFragment(mBookFragment,"Books");
        mAdapter.addFragment(mReadFragment,"Read");
        mAdapter.addFragment(mRandomFragment,"Random");
        this.mViewPager.setAdapter(mAdapter);
    }


    @Override
    public void onClick(int position) {
        mReadFragment.addItem(mBookFragment.getItem(position).getTitle());
        this.mViewPager.setCurrentItem(1);
    }
}