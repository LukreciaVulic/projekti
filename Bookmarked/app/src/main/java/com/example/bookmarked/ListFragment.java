package com.example.bookmarked;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


public class ListFragment extends Fragment{

    ListView listView;
    TextView textView;
    String[] titles= new String[]{};
    List<String> List= new ArrayList<>(Arrays.asList(titles));
    ArrayAdapter<String> adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        listView=view.findViewById(R.id.lvRead);
        textView=view.findViewById(R.id.textView);
        adapter=new ArrayAdapter<String>(this.getContext(),R.layout.list_item,R.id.textView,List);
        listView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    public void addItem(String book) {
        List.add(0,book);
        adapter.notifyDataSetChanged();
    }
}