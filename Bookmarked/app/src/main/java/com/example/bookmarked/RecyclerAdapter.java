package com.example.bookmarked;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private List<Book> dataList= new ArrayList<>();
    ButtonClickListener buttonClickListener;

    public RecyclerAdapter(ButtonClickListener listener){
        buttonClickListener=listener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.item_books,parent,false);
        return new ItemViewHolder(itemView,buttonClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.setItems(dataList.get(position));
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public void addData(List<Book> data){
        this.dataList.clear();
        this.dataList.addAll(data);
        notifyDataSetChanged();
    }

    public void addNewItem(Book item){
        dataList.add(item);
    }

    public Book getItem(int position){
        return dataList.get(position);
    }

    public void removeItem(int position){
        if(dataList.size()>position){
            dataList.remove(position);
            notifyItemRemoved(position);
        }
    }
}
