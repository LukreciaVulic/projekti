package com.example.bookmarked;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;


public class RandomFragment extends Fragment implements ButtonClickListener1 {

    private String lines[]={"Happy families are all alike; every unhappy family is unhappy in its own way.",
            "It is a truth universally acknowledged, that a single man in possession of a good fortune, must be in want of a wife.",
            "It was a bright cold day in April, and the clocks were striking thirteen.",
            "My father's family name being Pirrip, and my christian name Philip, my infant tongue could make of both names nothing longer or more explicit than Pip.",
            "I have just returned from a visit to my landlord – the solitary neighbour that I shall be troubled with.",
            "On an exceptionally hot evening early in July a young man came out of the garret in which he lodged in S. Place and walked slowly, as though in hesitation, towards K. bridge."
    };


    private String names[]={"Anna Karenina",
    "Pride and Prejudice","1984","Great Expectations","Wuthering Heights","Crime and Punishment"};
    private static final String BUNDLE_MESSAGE="mess";
    TextView tvChapter;
    Button btnRandom;
    Button btnBook;
    ButtonClickListener mButtonClickListenerRandom;
    ButtonClickListener mButtonClickListenerBook;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_random, container, false);
    }

    public static RandomFragment newInstance(String message){
        RandomFragment fragment= new RandomFragment();
        Bundle args=new Bundle();
        args.putString(BUNDLE_MESSAGE,message);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvChapter=view.findViewById(R.id.tvChapter);
        btnBook=view.findViewById(R.id.btnBook);
        btnRandom=view.findViewById(R.id.btnRandom);
        onClick();
    }


    @Override
    public void onClick() {
        btnRandom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random random= new Random();
                int index= random.nextInt(lines.length-0)+0;
                tvChapter.setText(lines[index]);
                btnBook.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getActivity(),names[index],Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
    }
}