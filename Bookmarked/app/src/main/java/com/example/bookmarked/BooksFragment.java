package com.example.bookmarked;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BooksFragment extends Fragment implements ButtonClickListener {

    private RecyclerView recyclerView;
    private RecyclerAdapter adapter;

    private Call<List<Book>> apiCall;

    Context context;

    ButtonClickListener btnClickRead;

    ListFragment readFragment;

    public BooksFragment(ListFragment readList){
        readFragment=readList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_books, container, false);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        adapter = new RecyclerAdapter(this::onClick);
        recyclerView.setAdapter(adapter);
        context = view.getContext();
        setUpApiCall();
        return view;
    }

    private void setUpApiCall() {
        apiCall=NetworkUtils.getApiInterface().getBooks();
        apiCall.enqueue(new Callback<List<Book>>() {
            @Override
            public void onResponse(Call<List<Book>> call, Response<List<Book>> response) {
                if(response.isSuccessful() && response.body()!=null){
                    setUpRecycler(response.body());
                }
            }

            @Override
            public void onFailure(Call<List<Book>> call, Throwable t) {
                Toast.makeText(context,"Error",Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setUpRecycler(List<Book> data) {
        adapter.addData(data);
    }

    public Book getItem(int position){
        return adapter.getItem(position);
    }

    public void onClick(int position) {
        readFragment.addItem(adapter.getItem(position).getTitle());
    }
}
