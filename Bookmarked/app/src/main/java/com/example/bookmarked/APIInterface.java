package com.example.bookmarked;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {
    @GET("LukreciaVulic/json/-/raw/master/json")
    Call<List<Book>> getBooks();
}
