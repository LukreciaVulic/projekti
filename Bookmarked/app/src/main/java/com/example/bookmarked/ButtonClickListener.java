package com.example.bookmarked;

import android.view.View;

public interface ButtonClickListener {
    void onClick(int position);
}
